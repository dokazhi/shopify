import 'package:flutter/material.dart';
import 'package:shopify/main.dart';


class ProductDetails extends StatefulWidget {
  final productDetailName;
  final productDetailNewPrice;
  final productDetailOldPrice;
  final productDetailPicture;

  ProductDetails({this.productDetailName, this.productDetailNewPrice, this.productDetailOldPrice, this.productDetailPicture});

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.1,
        backgroundColor: Colors.deepPurple,
        title: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context)=> new HomePage()));
          },
          child: Text('Shopify')
        ),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                  Icons.search,
                  color: Colors.white
              ),
              onPressed: (){}),
        ],
      ),
      body: new ListView(
        children: <Widget>[
          Container(
            height: 300,
            child: GridTile(
              
              child: Container(
                color: Colors.white70,
                child: Image.asset(widget.productDetailPicture)
              ),
              footer: Container(
                color: Colors.white,
                child: ListTile(
                  leading: Text(
                      widget.productDetailName,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 16.0
                      ),
                  ),
                  title: new Row(
                    children: <Widget>[
                      Expanded(child: Text(
                          "\$${widget.productDetailOldPrice}",
                          style: TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.lineThrough),
                          )
                      ),
                      Expanded(child: Text(
                          "\$${widget.productDetailNewPrice}",
                          style: TextStyle(
                            color: Colors.red,
                            fontWeight: FontWeight.bold
                          ),
                        )
                      )
                    ],
                  )
                )
              ),
            ),
          ),
// Teacher says we need comments, BUT WE NEED FUCKING REFACTOR THIS SPAGHETTI SHIT

          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: (){
                    showDialog(
                      context: context,
                      builder: (context){
                        return AlertDialog(
                          title: Text('Size'),
                          content: Text("Choose the size"),
                          actions: <Widget>[
                            MaterialButton(
                              onPressed: (){
                                Navigator.of(context).pop(context);
                              },
                              child: new Text("close"),
                            )
                          ],
                        );
                      }
                    );
                  },
                  color: Colors.white,
                  textColor: Colors.grey,
                  elevation: 0.2,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text("Size")
                      )  ,
                      Expanded(
                        child: Icon(Icons.arrow_drop_down)
                      )
                    ],
                  )
                  ),
              ),
              Expanded(
                child: MaterialButton(
                    onPressed: (){
                      showDialog(
                          context: context,
                          builder: (context){
                            return AlertDialog(
                              title: Text('Color'),
                              content: Text("Choose the color"),
                              actions: <Widget>[
                                MaterialButton(
                                  onPressed: (){
                                    Navigator.of(context).pop(context);
                                  },
                                  child: new Text("close"),
                                )
                              ],
                            );
                          }
                      );
                    },
                    color: Colors.white,
                    textColor: Colors.grey,
                    elevation: 0.2,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text("Color")
                        )  ,
                        Expanded(
                            child: Icon(Icons.arrow_drop_down)
                        )
                      ],
                    )
                ),
              ),
              Expanded(
                child: MaterialButton(
                    onPressed: (){
                      showDialog(
                          context: context,
                          builder: (context){
                            return AlertDialog(
                              title: Text('Quantity'),
                              content: Text("Choose the qunatity"),
                              actions: <Widget>[
                                MaterialButton(
                                  onPressed: (){
                                    Navigator.of(context).pop(context);
                                  },
                                  child: new Text("close"),
                                )
                              ],
                            );
                          }
                      );
                    },
                    color: Colors.white,
                    textColor: Colors.grey,
                    elevation: 0.2,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                            child: Text("Qty")
                        )  ,
                        Expanded(
                            child: Icon(Icons.arrow_drop_down)
                        )
                      ],
                    )
                ),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: MaterialButton(
                  onPressed: (){},
                  color: Colors.red,
                  textColor: Colors.white,
                  elevation: 0.2,
                  child: Text("Buy now")
                ),
              ),
              new IconButton(icon: Icon(Icons.add_shopping_cart, color: Colors.red), onPressed: (){}),
              new IconButton(icon: Icon(Icons.favorite_border, color: Colors.red), onPressed: (){})
            ],
          ),
          Divider(color: Colors.grey),
          ListTile(
            title: Text("Product description"),
            subtitle: Text("""
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada felis vitae posuere commodo. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Suspendisse auctor lobortis nunc, eget aliquam ligula aliquet sed. Nulla tortor nisl, vestibulum vel nulla nec, sollicitudin eleifend urna. Morbi non consectetur justo. Nulla nec interdum lorem. Vestibulum porta leo nulla, a suscipit tortor faucibus vel.
              Nulla mauris ligula, pellentesque quis elit vel, viverra finibus nisi. Duis efficitur non nisl sed rutrum. In sed erat et magna iaculis hendrerit nec egestas sem. Donec sodales mauris ligula, ac sodales erat lobortis nec. Praesent viverra sollicitudin dui, id tincidunt augue laoreet sed. Aenean ut risus nunc. Morbi rhoncus lobortis elit eu rutrum. Vestibulum porta, velit a facilisis pretium, purus nunc sodales lorem, id eleifend est odio ut ante. Praesent at lorem dictum, congue quam nec, hendrerit diam. Ut quam neque, aliquet et lacinia quis, elementum ut orci. """),
          ),
          Divider(),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text("Product name", style: TextStyle(color: Colors.grey),),

              ),
              Padding(
                  padding: EdgeInsets.all(5.0),
                  child: Text(widget.productDetailName)
              )
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text("Product brand", style: TextStyle(color: Colors.grey),),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("Brand Y")
              ),
            ]
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                child: Text("Product condition", style: TextStyle(color: Colors.grey),),
              ),
              Padding(
                padding: EdgeInsets.all(5.0),
                child: Text("Some condition")
              ),
            ],
          ),

          Divider(),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text('Similar Products'),
          ),

          Container(
            height: 400.0,
            child: SimilarProducts(),
          )

        ]
      )
    );
  }
}


class SimilarProducts extends StatefulWidget {
  @override
  _SimilarProductsState createState() => _SimilarProductsState();
}

class _SimilarProductsState extends State<SimilarProducts> {
  var productList = [
    {
      'name': 'Blazer',
      'picture': 'images/rbf.jpeg',
      'old_price': 3.99,
      'price': 2.99
    },
    {
      'name': 'Jacket',
      'picture': 'images/bhwc.jpg',
      'old_price': 5.00,
      'price': 4.29
    },
    {
      'name': 'Fucked',
      'picture': 'images/rbf.jpeg',
      'old_price': 5.00,
      'price': 4.29
    },
    {
      'name': 'Ducked',
      'picture': 'images/bhwc.jpg',
      'old_price': 56.00,
      'price': 34.29
    },

  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: productList.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index){
          return SimilarSingleProduct(
            productName: productList[index]['name'],
            productPicture: productList[index]['picture'],
            productPrice: productList[index]['price'],
            productOldPrice: productList[index]['old_price'],
          );
        }
    );
  }
}

class SimilarSingleProduct extends StatelessWidget {
  final productName;
  final productPicture;
  final productOldPrice;
  final productPrice;

  SimilarSingleProduct({
    this.productName,
    this.productPicture,
    this.productOldPrice,
    this.productPrice
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: productName,
          child: Material(
              child:InkWell(
                onTap: ()=> Navigator.of(
                    context
                ).push(
                    new MaterialPageRoute(
                        builder: (context) => new ProductDetails(
                          productDetailName: productName,
                          productDetailNewPrice: productPrice,
                          productDetailOldPrice: productOldPrice,
                          productDetailPicture: productPicture,
                        )
                    )
                ),
                child: GridTile(
                  footer: Container(
                      color: Colors.white,
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              productName,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16.0
                              ),
                            ),
                          ),
                          Text(
                              "\$$productPrice",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold
                              )
                          )
                        ],
                      )
                  ),
                  child: Image.asset(productPicture, fit: BoxFit.cover),
                ),
              )
          )
      ),
    );
  }
}
