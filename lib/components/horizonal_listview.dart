import 'package:flutter/material.dart';


//never changed widget(btw if we use admin page or something for control it will be resized)
class HorizontalList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Category(
            image_location: 'images/png/001-one.png',
            image_caption: '001',
          ),
          Category(
            image_location: 'images/png/002-two.png',
            image_caption: '002',
          ),
          Category(
            image_location: 'images/png/003-three.png',
            image_caption: '003',
          ),
          Category(
            image_location: 'images/png/004-four.png',
            image_caption: '004',
          ),
          Category(
            image_location: 'images/png/005-five.png',
            image_caption: '005',
          ),
          Category(
            image_location: 'images/png/006-six.png',
            image_caption: '006',
          ),
          Category(
            image_location: 'images/png/007-rotate.png',
            image_caption: '007',
          ),
          Category(
            image_location: 'images/png/008-bandage.png',
            image_caption: '008',
          ),
        ],
      ),
    );
  }
}

class Category extends StatelessWidget {
  final String image_location;
  final String image_caption;

  Category({
    this.image_location,
    this.image_caption
  });

  @override
  Widget build(BuildContext context) {
    return Padding(padding: const EdgeInsets.all(2.0),
    child: InkWell(
      onTap: (){},
      child: Container(
        width: 100.0,
        child:
          ListTile(
            title: Image.asset(
              image_location,
              width: 100.0,
              height: 80.0
            ),
            subtitle: Container(
              alignment: Alignment.topCenter,
              child: Text(image_caption, style: TextStyle(fontSize: 12.0),),
            ),
          ),
      ),
    ),
    );
  }
}

