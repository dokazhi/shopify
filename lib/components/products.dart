import 'package:flutter/material.dart';


import 'package:shopify/pages/product_details.dart';


class Products extends StatefulWidget {
  @override
  _ProductsState createState() => _ProductsState();
}

class _ProductsState extends State<Products> {
  var productList = [
    {
      'name': 'Blazer',
      'picture': 'images/rbf.jpeg',
      'old_price': 3.99,
      'price': 2.99
    },
    {
      'name': 'Jacket',
      'picture': 'images/bhwc.jpg',
      'old_price': 5.00,
      'price': 4.29
    },
    {
      'name': 'Fucked',
      'picture': 'images/rbf.jpeg',
      'old_price': 5.00,
      'price': 4.29
    },
    {
      'name': 'Ducked',
      'picture': 'images/bhwc.jpg',
      'old_price': 56.00,
      'price': 34.29
    },
    {
      'name': 'Sucked',
      'picture': 'images/rbf.jpeg',
      'old_price': 15.00,
      'price': 34.29
    },{
      'name': 'Junked',
      'picture': 'images/bhwc.jpg',
      'old_price': 15.00,
      'price': 64.29
    }
  ];
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: productList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index){
        return SingleProduct(
          productName: productList[index]['name'],
          productPicture: productList[index]['picture'],
          productPrice: productList[index]['price'],
          productOldPrice: productList[index]['old_price'],
        );
      }
    );
  }
}

class SingleProduct extends StatelessWidget {
  final productName;
  final productPicture;
  final productOldPrice;
  final productPrice;

  SingleProduct({
    this.productName,
    this.productPicture,
    this.productOldPrice,
    this.productPrice
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: productName,
          child: Material(
              child:InkWell(
                onTap: ()=> Navigator.of(
                    context
                ).push(
                    new MaterialPageRoute(
                        builder: (context) => new ProductDetails(
                          productDetailName: productName,
                          productDetailNewPrice: productPrice,
                          productDetailOldPrice: productOldPrice,
                          productDetailPicture: productPicture,
                        )
                    )
                ),
                child: GridTile(
                  footer: Container(
                    color: Colors.white,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            productName,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0
                            ),
                          ),
                        ),
                        Text(
                            "\$$productPrice",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold
                            )
                        )
                      ],
                    )
                  ),
                  child: Image.asset(productPicture, fit: BoxFit.cover),
                ),
              )
          )
      ),
    );
  }
}
